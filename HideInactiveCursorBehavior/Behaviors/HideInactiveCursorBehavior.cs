﻿namespace agrygor.Behaviors
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;

    /// <summary>
    /// Represents a behavior which hide inactive cursor after some interval.
    /// </summary>
    public class HideInactiveCursorBehavior
    {
        #region IntervalProperty

        /// <summary>
        /// Time interval after which cursor should be hided.
        /// </summary>
        public static readonly DependencyProperty IntervalProperty = DependencyProperty.RegisterAttached(
           "Interval",
           typeof(TimeSpan),
           typeof(HideInactiveCursorBehavior),
           new PropertyMetadata(TimeSpan.FromSeconds(1), OnIntervalChanged));

        public static TimeSpan GetInterval(DependencyObject obj)
        {
            return (TimeSpan)obj.GetValue(IntervalProperty);
        }

        public static void SetInterval(DependencyObject obj, TimeSpan value)
        {
            obj.SetValue(IntervalProperty, value);
        }

        private static void OnIntervalChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var element = sender as FrameworkElement;
            if (element == null)
            {
                return;
            }

            if (!GetIsEnabled(element))
            {
                return;
            }

            if (_activityTimer != null)
            {
                _activityTimer.Interval = (TimeSpan)e.NewValue;
            }
        }

        #endregion

        #region IsEnabledProperty

        /// <summary>
        /// Enable or disable behavior functionality.
        /// </summary>
        public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.RegisterAttached(
            "IsEnabled",
            typeof(bool),
            typeof(HideInactiveCursorBehavior),
            new PropertyMetadata(false, OnIsEnabledChanged));

        public static bool GetIsEnabled(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsEnabledProperty);
        }

        public static void SetIsEnabled(DependencyObject obj, bool value)
        {
            obj.SetValue(IsEnabledProperty, value);
        }

        private static void OnIsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var element = sender as FrameworkElement;
            if (element == null)
            {
                return;
            }

            if (GetIsEnabled(element))
            {
                OnAttached(element);
            }
            else
            {
                OnDetaching(element);
            }
        }

        #endregion

        #region InactiveCursorProperty

        /// <summary>
        /// Cursor type when it's not active.
        /// </summary>
        public static readonly DependencyProperty InactiveCursorProperty = DependencyProperty.RegisterAttached(
          "InactiveCursor",
          typeof(Cursor),
          typeof(HideInactiveCursorBehavior),
          new PropertyMetadata(Cursors.None));

        public static Cursor GetInactiveCursor(DependencyObject obj)
        {
            return (Cursor)obj.GetValue(InactiveCursorProperty);
        }

        public static void SetInactiveCursor(DependencyObject obj, Cursor value)
        {
            obj.SetValue(InactiveCursorProperty, value);
        }

        #endregion

        #region ActiveCursorProperty

        /// <summary>
        /// Cursor type when it's active.
        /// </summary>
        public static readonly DependencyProperty ActiveCursorProperty = DependencyProperty.RegisterAttached(
          "ActiveCursor",
          typeof(Cursor),
          typeof(HideInactiveCursorBehavior),
          new PropertyMetadata(Cursors.Arrow));

        public static Cursor GetActiveCursor(DependencyObject obj)
        {
            return (Cursor)obj.GetValue(ActiveCursorProperty);
        }

        public static void SetActiveCursor(DependencyObject obj, Cursor value)
        {
            obj.SetValue(ActiveCursorProperty, value);
        }

        #endregion

        /// <summary>
        /// Represents a timer which countdown time interval.
        /// </summary>
        private static DispatcherTimer _activityTimer;

        /// <summary>
        /// Attaches behavior for element.
        /// </summary>
        /// <param name="element">Element where behavior should be attached.</param>
        private static void OnAttached(FrameworkElement element)
        {
            HideCursor(element);

            if (_activityTimer == null)
            {
                _activityTimer = new DispatcherTimer { IsEnabled = true };
                _activityTimer.Tick += (s, e1) => HideCursor(element);

                element.MouseMove += ElementMouseMove;
            }

            _activityTimer.Interval = GetInterval(element);
        }

        /// <summary>
        /// Detaches behavior for element.
        /// </summary>
        /// <param name="element">Element where behavior should be detached.</param>
        private static void OnDetaching(FrameworkElement element)
        {
            _activityTimer.Stop();
            _activityTimer = null;

            element.MouseMove -= ElementMouseMove;

            ShowCursor(element);
        }

        /// <summary>
        /// Fires when cursor was moved.
        /// </summary>
        /// <param name="sender">Element sender.</param>
        /// <param name="e">Mouse event arguments.</param>
        private static void ElementMouseMove(object sender, MouseEventArgs e)
        {
            ShowCursor((FrameworkElement)sender);

            _activityTimer.Stop();
            _activityTimer.Start();
        }

        /// <summary>
        /// Method encapsulate showing cursor logic for specific element.
        /// </summary>
        /// <param name="element">Element which cursor should be set to active.</param>
        private static void ShowCursor(FrameworkElement element)
        {
            element.Cursor = GetActiveCursor(element);
        }

        /// <summary>
        /// Method encapsulate hiding cursor logic for specific element.
        /// </summary>
        /// <param name="element">Element which cursor should be set to inactive.</param>
        private static void HideCursor(FrameworkElement element)
        {
            element.Cursor = GetInactiveCursor(element);
        }
    }
}
